import React from "react";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import InputBase from "@material-ui/core/InputBase";

class NavBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      keyword: "food"
    };
  }

  render() {
    const { getResults } = this.props;
    return (
      <AppBar style={styles.appBarStyle} className="App-bar">
        <h1>React Google Api Demo</h1>
        <InputBase
          style={styles.inputBase}
          onChange={event => this.setState({ keyword: event.target.value })}
          placeholder="  Search food..."
        />
        <Button
          value={this.state.keyword}
          style={{ marginLeft: 20, backgroundColor: "#ff3726" }}
          variant="contained"
          color="secondary"
          onClick={() => {
            getResults(this.state.keyword);
          }}
        >
          Go
        </Button>
      </AppBar>
    );
  }
}
const styles = {
  appBarStyle: {
    backgroundColor: "#112F68",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  inputBase: {
    maxWidth: 500,
    minWidth: 200,
    marginLeft: 50,
    borderRadius: 6,
    backgroundColor: "white"
  }
};
export default NavBar;
