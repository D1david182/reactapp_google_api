import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import React, { Component, StyleSheet } from "react";

export class MapContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      results: [],
      loading: false,
      resultName: null
    };
    this.onMarkerClick = this.onMarkerClick.bind(this);
  }

  onMarkerClick(location) {
    this.setState({
      resultName: location.name
    });
  }
  render() {
    if (!this.props.results) return <p> There are no results </p>;
    let { results } = this.props.results;
    return (
      <div style={styles.divStyle}>
        <Map
          ref="map"
          google={this.props.google}
          zoom={14}
          initialCenter={{
            lat: 33.430412,
            lng: -111.9395
          }}
        >
          {results.map(marker => (
            <Marker
              onClick={this.onMarkerClick}
              name={marker.name}
              position={{
                lat: marker.geometry.location.lat,
                lng: marker.geometry.location.lng
              }}
              key={marker.id}
            />
          ))}
          <Marker onClick={this.onMarkerClick} name={"Zenefits"} />
        </Map>
        {this.state.resultName && (
          <h3 style={styles.headerStyle}>Location: {this.state.resultName}</h3>
        )}
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyDzSbX8xCkMX6egmT-2dFCTYm4DpYOP734"
})(MapContainer);

const styles = {
  headerStyle: {
    position: "absolute",
    marginLeft: 300,
    color: "#112F68",
    fontSize: 15,
    alignText: "center"
  },
  divStyle: {
    alignItems: "center",
    justifyItems: "center"
  }
};
