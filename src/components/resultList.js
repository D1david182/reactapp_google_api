import React, { Component } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";

class ResultList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      results: [],
      loading: false
    };
  }

  render() {
    if (!this.props.results) return <p> There are no results </p>;
    else {
      let { results } = this.props.results;

      return (
        <React.Fragment>
          <List style={styles.listStyle}>
            {this.state.loading === false &&
              results.map(result => (
                <ListItem
                  key={result.id}
                  style={styles.listItem}
                  alignItems="flex-start"
                >
                  <ListItemAvatar>
                    <Avatar
                      alt="Remy Sharp"
                      src={require("../assets/mapPin.png")}
                    />
                  </ListItemAvatar>
                  <ListItemText
                    primary={
                      <Typography type="body2" style={{ color: "#ff3726" }}>
                        {result.name}
                      </Typography>
                    }
                    secondary={
                      <React.Fragment>
                        <Typography
                          component="span"
                          style={{ display: "Subheading" }}
                          color="textPrimary"
                        >
                          {result.vicinity}
                        </Typography>
                        Rating: {result.rating}
                      </React.Fragment>
                    }
                  />
                </ListItem>
              ))}
          </List>
        </React.Fragment>
      );
    }
  }
}
const styles = {
  listStyle: {
    marginLeft: 20,
    maxHeight: "65vh",
    maxWidth: "50vw",
    minHeight: "45vh",
    minWidth: "40vw",
    overflow: "scroll"
  },
  listItem: {
    backgroundColor: "white",
    borderRadius: 6,
    marginTop: 10
  }
};
export default ResultList;
