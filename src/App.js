import React, { Component } from "react";
import NavBar from "./components/navBar";
import LoadingWheel from "./components/loadingWheel";
import "./App.css";
import ResultList from "./components/resultList";
import MapContainer from "./components/mapComponent";
import axios from "axios";

let defaultLat = 33.430461;
let defaultLong = -111.939455;
let apiKey = "AIzaSyDZsUt7KmBEyLnSomCVQ3713rdD_NAvC4s";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      results: [],
      loading: true,
      keyword: "food"
    };
    this.getResults = this.getResults.bind(this);
  }

  async getResults(keyword) {
    keyword = keyword.toLowerCase();
    this.setState({ loading: true });
    const API = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${defaultLat},${defaultLong}&radius=1500&type=restaurant&keyword=${keyword}&key=${apiKey}`;
    axios
      .get("https://cors-anywhere.herokuapp.com/" + API)
      .then(async result => {
        this.setState({ results: result.data.results, loading: false });
      })
      .catch(error => {
        console.log("Error: ", error);
        this.setState({ results: [], loading: false });
      });
  }

  componentDidMount() {
    this.getResults("food");
  }
  render() {
    return (
      <div className="App-header">
        <header className="App-header">
          <NavBar getResults={this.getResults} />
          {this.state.loading === true && (
            <LoadingWheel type="spin" color="white" />
          )}
          {this.state.loading === false && <ResultList results={this.state} />}
          <div style={styles.mapStyle}>
            {this.state.loading === false && (
              <MapContainer results={this.state}> </MapContainer>
            )}
          </div>
        </header>
      </div>
    );
  }
}
const styles = {
  mapStyle: {
    marginLeft: 50,
    marginTop: 20,
    backgroundColor: "#112F68",
    height: "65vh",
    width: "50vw",
    position: "relative"
  }
};
export default App;
